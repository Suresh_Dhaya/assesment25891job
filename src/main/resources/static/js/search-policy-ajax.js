/**
 * 
 */
$(document).ready(function(){
	$("#get-policy").on("submit",function(event){
		console.log("Inside on submit event");
		event.preventDefault();
		
		formData=$("#get-policy").serialize();
		console.log(formData);
		$("#policynotexist").text("");
		
		$.ajax({
				
				type:"POST",
				url:"http://localhost:8081/springbootdemo/searchPolicyAJAX",
				data:formData,
				success:function(data,status,xhr){
					$("input[name=policyNo]").val(data.policyNo);
					$("input[name=premium]").val(data.premium);
					$("input[name=policymode]").val(data.policyMode);
					$("input[name=startdate]").val(data.startDate);
				},
				error:function(){
					
					$("input[name=premium]").val("");
					$("input[name=policymode]").val("");
					$("input[name=startdate]").val("");
					$("#policynotexist").css("color","red");
					$("#policynotexist").text("policy id not exist");
				}
		});
	});
});